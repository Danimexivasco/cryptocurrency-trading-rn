import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableHighlight, Alert } from 'react-native';
import { Picker } from '@react-native-community/picker';
import axios from 'axios';

const Form = ({currency, cryptoCurrency, setCurrency, setCryptoCurrency, setConsultAPI}) => {

  
    const [cryptoCurrencies, setCryptoCurrencies] = useState([]);

    useEffect(() => {
        const consultAPI = async () => {
            const url = 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD'
            const result = await axios.get(url);
            setCryptoCurrencies(result.data.Data)
        }
        consultAPI();
    }, [])


    const getCurrency = currency => {
        setCurrency(currency)
    }

    const getCrypto = crypto => {
        setCryptoCurrency(crypto)
    }

    const tradePrice = () => {
        if(currency.trim() === '' || cryptoCurrency.trim() === ''){
            showAlert();
            return;
        }

        // Validation passes
        setConsultAPI(true);
    }

    const showAlert = () => {
        Alert.alert(
            'Error',
            'All fields are required',
            [
                {text: 'OK'}
            ]
        )
    }

    return (
        <View>
            <Text style={styles.label}>Currency</Text>
            <Picker
                onValueChange={currency => getCurrency(currency)}
                selectedValue={currency}
                itemStyle={{ height: 120 }}
                style = {styles.picker}
            >
                <Picker.Item label="-- Select --" value="" />
                <Picker.Item label="USA Dolar" value="USD" />
                <Picker.Item label="Mexican Peso" value="MXN" />
                <Picker.Item label="Euro" value="EUR" />
                <Picker.Item label="Pund Sterling" value="GBP" />
            </Picker>


            <Text style={styles.label}>Cryptocurrency</Text>
            <Picker
                onValueChange={crypto => getCrypto(crypto)}
                selectedValue={cryptoCurrency}
                itemStyle={{ height: 120 }}
                style = {styles.picker}
            >
                <Picker.Item label="-- Select --" value="" />
                {cryptoCurrencies.map(crypto => (
                    <Picker.Item key={crypto.CoinInfo.Id} label={crypto.CoinInfo.FullName} value={crypto.CoinInfo.Name} />
                ))}

            </Picker>

            <TouchableHighlight
                style={styles.btnTrade}
                onPress = { () => tradePrice()}
            >
                <Text style={styles.txtTrade}>Trade</Text>
            </TouchableHighlight>
        </View>
    );
}

const styles = StyleSheet.create({
    label: {
        fontFamily: 'Lato-Black',
        textTransform: 'uppercase',
        fontSize: 22,
        marginVertical: 20,
        color: '#FFF',
    },
    btnTrade: {
        backgroundColor: '#5E49E2',
        padding: 10,
        marginTop: 40
    },
    txtTrade: {
        color: '#FFF',
        fontSize: 18,
        fontFamily: 'Lato-Black',
        textTransform: 'uppercase',
        textAlign: 'center'
    },
    picker: {
        color: '#FFF',
        // borderBottomColor: '#FFF',
        // borderBottomWidth: 2,
        // borderStyle: 'solid',
    }
});


export default Form;