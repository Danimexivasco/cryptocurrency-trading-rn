import React from 'react';
import { StyleSheet, View, Text } from 'react-native'

const Trade = ({result}) => {
    

    if(Object.keys(result).length === 0) return null;

    return ( 
        <View style={ result.CHANGEPCT24HOUR < 0 ? styles.badTrade : styles.goodTrade}>
            <Text style={[styles.txt, styles.price]}> Price: {' '}
                <Text style={styles.span}>{result.PRICE}</Text>
            </Text>
            <Text style={styles.txt}> Highest price of the day: {' '}
                <Text style={styles.span}>{result.HIGHDAY}</Text>
            </Text>
            <Text style={styles.txt}> Lowest price of the day: {' '}
                <Text style={styles.span}>{result.LOWDAY}</Text>
            </Text>
            <Text style={styles.txt}> Variation las 24H: {' '}
                <Text style={styles.span}>{result.CHANGEPCT24HOUR} %</Text>
            </Text>
            <Text style={styles.txt}> Last Update: {' '}
                <Text style={styles.span}>{result.LASTUPDATE}</Text>
            </Text>
        </View>
     );
}

const styles = StyleSheet.create({
    goodTrade: {
        backgroundColor: '#74A202',
        padding: 20,
        // marginTop: 20
    },
    badTrade: {
        backgroundColor: '#ab2a2a',
        padding: 20,
        // marginTop: 20
    },
    txt: {
        color: '#FFF',
        fontFamily: 'Lato-Regular',
        fontSize: 18,
        marginBottom: 10
    },
    price: {
        fontSize: 38
    },
    span: {
        fontFamily: 'Lato-Black'
    }

});
 
export default Trade;