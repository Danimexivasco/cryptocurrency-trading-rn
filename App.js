
import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, View, ScrollView, ActivityIndicator } from 'react-native';
import Header from './components/Header';
import Form from './components/Form';
import Trade from './components/Trade';
import axios from 'axios';

const App = () => {

  const [currency, setCurrency] = useState('');
  const [cryptoCurrency, setCryptoCurrency] = useState('');
  const [consultAPI, setConsultAPI] = useState(false);
  const [result, setResult] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    console.log(consultAPI)
    const tradeCryptoCurrency = async () => {
      if (consultAPI) {
        console.log('dentro')
        // Consult API for get the Trade price
        const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${cryptoCurrency}&tsyms=${currency}`
        const result = await axios.get(url)

        setLoading(true);

        // Delay the response
        setTimeout(() => {
          setResult(result.data.DISPLAY[cryptoCurrency][currency])
          setConsultAPI(false)
          setLoading(false)
        }, 2000)
        // setResult(result.data.DISPLAY[cryptoCurrency][currency])
        // setConsultAPI(false)
      }
    }
    tradeCryptoCurrency();
  }, [consultAPI]);

  const component = loading ? <ActivityIndicator size="large" color="#5E49E2"/> : <Trade result={result} />

  return (
    <>
      <ScrollView
        style={{ backgroundColor: '#363230' }}
      >
        <Header />
        <Image
          style={styles.image}
          source={require('./assets/img/cryptomonedas.png')}
        />
        <View style={styles.container}>
          <Form
            currency={currency}
            cryptoCurrency={cryptoCurrency}
            setCurrency={setCurrency}
            setCryptoCurrency={setCryptoCurrency}
            setConsultAPI={setConsultAPI}
          />
        </View>
        {/* <Trade
          result={result}
        /> */}
        <View style={{marginTop: 40}}>
          {component}
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  image: {
    width: '95%',
    height: 150,
    marginHorizontal: '2.5%',
  },
  container: {
    marginHorizontal: '2.5%'
  }
});

export default App;
